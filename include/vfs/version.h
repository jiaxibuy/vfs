#ifndef _VFS_VERSION_H_
#define _VFS_VERSION_H_

/************************************************************************/
/* 版本定义 */
#define VFS_SDK_VERSION   "1.2.0"

#define VFS_VERSION_MAJOR      1
#define VFS_VERSION_MINOR      2
#define VFS_VERSION_REVISION   0

#endif
